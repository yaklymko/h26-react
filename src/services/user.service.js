
class UserService{
	getCurrUser(){
		return JSON.parse(localStorage.getItem("currUser"));
	}

	setCurrUser(id, username){
		localStorage.setItem("currUser", JSON.stringify({id, username}));
	}

}

export default new UserService();