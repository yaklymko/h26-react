import React from "react";
import MessageCard from "./MessageCard/MessageCard";
import "./MessagesList.css";

export default function MessagesList(props) {
	let prevDay = null;

	return (<div className={"messages-wrapper"}>
		{props.messages.map(mess => {
			const currMessDay = (new Date(mess.createdAt)).getDay();

			if (prevDay === null || currMessDay !== prevDay) {
				prevDay = currMessDay;
				return (
					<>
						{dayLine(new Date(mess.createdAt))}
						<MessageCard key={mess.createdAt} message={mess} userId={props.userId} deleteMessage={props.deleteMessage} key={mess.id}
									 turnEditMode={props.turnEditMode}/>
					</>
				);
			}

			return <MessageCard message={mess} userId={props.userId} deleteMessage={props.deleteMessage} key={mess.id}
				turnEditMode={props.turnEditMode}/>;
		})}
	</div>);
}

function dayLine(dayDate) {
	return (
		<div className={"align-center"} key={dayDate}>
			{dayDate.toDateString()}
		</div>
	);
}