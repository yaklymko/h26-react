import React from "react";
import ReactDOM from "react-dom";

import Chat from "./components/Chat/Chat";
import Header from "./components/partials/Header/Header";
import Footer from "./components/partials/Footer/Footer";

import UserService from "./services/user.service";
import "./index.css";

UserService.setCurrUser(1, "you");

ReactDOM.render(
	<React.StrictMode>
		<Header />
		<Chat/>
		<Footer/>
	</React.StrictMode>,
	document.getElementById("root")
);
