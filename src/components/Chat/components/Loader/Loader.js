import React from "react";
import "./Loader.css";

export default function Loader(props){
	return(<div id={"loader"}>
		<img src="./loader.gif" alt=""/>
	</div>);
}