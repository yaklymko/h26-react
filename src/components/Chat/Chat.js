import React from "react";

import Header from "./components/Header/Header";
import MessagesList from "./components/MessagesList/MessagesList";
import EditForm from "./components/EditForm/EditMessageForm";
import CreateMessageForm from "./components/CreateMessageForm/CreateMessageForm";
import Loader from "./components/Loader/Loader";

import UserService from "../../services/user.service";
import MessageService from "../../services/message.service";

import "./Chat.css";

class Chat extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			messages: [],
			isLoader: true,
			editMode: false,
		};
	}

	componentDidMount() {

		fetch("https://edikdolynskyi.github.io/react_sources/messages.json", {
			method: "GET"
		}).then(res => res.json()).then(parsedRes => {
			parsedRes.forEach(mess => mess.likes = []);
			this.setState({messages: parsedRes, isLoader: false});
		});
	}

	scrollChat = () => {
		const chatBox = document.getElementsByClassName("messages-wrapper")[0];
		chatBox.scrollTop = chatBox.scrollHeight;
	};

	addMessage = (text) => {
		const currUser = UserService.getCurrUser();
		const newMessage = MessageService.createMessage(text, currUser.username, currUser.id);
		const messagesArr = this.state.messages;

		messagesArr.push(newMessage);

		this.setState({
			messages: messagesArr
		});
	};

	editMessage = (id, text) => {
		const targetMessage = this.state.messages.find(mess => mess.id === id);
		targetMessage.text = text;
		targetMessage.editedAt = (new Date()).toISOString();
		this.setState({editMode: false});
	};

	deleteMessage = (messageId) => {
		const newMessagesArr = this.state.messages.filter(m => m.id !== messageId);
		this.setState({
			messages: newMessagesArr
		});
	};

	render() {
		const messagesArr = this.state.messages;
		const participants = new Set(messagesArr.map(m => m.user));
		const currUser = UserService.getCurrUser();

		const lastMessageAt = messagesArr.length ? messagesArr[messagesArr.length - 1].createdAt : null;

		return (

			<main id={"chatGame"}>
				{this.state.isLoader ? <Loader/> : ""}
				<Header chatName={"My chat"}
					participantsAmount={participants.size}
					messagesAmount={messagesArr.length}
					lastMessageTime={lastMessageAt || (new Date).toString()}
				/>
				<MessagesList messages={messagesArr} userId={currUser.id} deleteMessage={this.deleteMessage}
							  turnEditMode={(id, text) => this.setState({editMode: {id: id, text: text}})}/>

				{this.state.editMode ?
					<EditForm prevText={this.state.editMode.text}
							  editMessage={(text) => this.editMessage(this.state.editMode.id, text)}
							  cancelEdit={() => this.setState({editMode: false})}
							  scrollChat={this.scrollChat}/>
					:
					<CreateMessageForm createMessage={this.addMessage} scrollChat={this.scrollChat}/>
				}
			</main>
		);
	}
}

export default Chat;