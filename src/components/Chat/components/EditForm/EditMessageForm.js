import React from "react";
import "./EditMessageForm.css";

class EditMessageForm extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			message: props.prevText,
		};
	}

	handleChange = (event, name) => {
		this.setState({
			[name]: event.target.value,
		});
	};

	render() {

		return (
			<div className={"message-edit-form"}>
				<textarea placeholder={"Your message..."} name={"message"} value={this.state.message}
						  onChange={(e) => this.handleChange(e, "message")}>
				</textarea>


				<button onClick={() => {
					this.props.editMessage(this.state.message);
					this.setState({message: ""});
				}}>
					Edit message!
				</button>

				<button onClick={() => {
					this.props.cancelEdit();
					this.setState({message: ""});
				}}>
					Cancel Editing
				</button>


				<button onClick={this.props.scrollChat}>
					Scroll down!
				</button>
			</div>

		);
	}
}

export default EditMessageForm;